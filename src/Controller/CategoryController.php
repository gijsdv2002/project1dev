<?php


namespace App\Controller;


use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categorie")
 */
class CategoryController extends AbstractController
{
    private $csvConverter;
    public function __construct(indexController $csvConverter)
    {
        $this->csvConverter = $csvConverter;
    }

    /**
     * @Route("/", name="category_index", methods={"GET","POST"})
     */
    public function categoryIndex(CategoryRepository $categoryRepository): Response
    {
        $cat = $categoryRepository->findAllCategory();
        $title[] = ['name'=>'categorieën'];
        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($cat,$title);
        }

        return $this->render('user/category.html.twig', [
            'categories' => $cat
        ]);
    }
    /**
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     * @Route("/1", name="cat_1", methods={"GET","POST"})
     */
    public function cat1(UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        $user = $userRepository->findOnebyCategory(1);
        $title = $categoryRepository->findCatName(1);
        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($user, $title);
        }

        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);
    }
    /**
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     * @Route("/2", name="cat_2", methods={"GET","POST"})
     */
    public function cat2(UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        $user = $userRepository->findOnebyCategory(2);
        $title = $categoryRepository->findCatName(2);
        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($user, $title);
        }

        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);
    }
    /**
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     * @Route("/3", name="cat_3", methods={"GET","POST"})
     */
    public function cat3(UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        $user = $userRepository->findOnebyCategory(3);
        $title = $categoryRepository->findCatName(3);
        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($user, $title);
        }

        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);
    }
    /**
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     * @Route("/4", name="cat_4", methods={"GET","POST"})
     */
    public function cat4(UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        $user = $userRepository->findOnebyCategory(4);
        $title = $categoryRepository->findCatName(4);
        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($user, $title);
        }

        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);
    }
    /**
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     * @Route("/5", name="cat_5", methods={"GET","POST"})
     */
    public function cat5(UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        $user = $userRepository->findOnebyCategory(5);
        $title = $categoryRepository->findCatName(5);
        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($user, $title);
        }

        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);
    }
    /**
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     * @Route("/6", name="cat_6",methods={"GET","POST"})
     */
    public function cat6(UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        $user = $userRepository->findOnebyCategory(6);
        $title = $categoryRepository->findCatName(6);
        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($user, $title);
        }

        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);
    }
    /**
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     * @Route("/7", name="cat_7", methods={"GET","POST"})
     */
    public function cat7(UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        $user = $userRepository->findOnebyCategory(7);
        $title = $categoryRepository->findCatName(7);
        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($user, $title);
        }

        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);
    }
    /**
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     * @Route("/8", name="cat_8", methods={"GET","POST"})
     */
    public function cat8(UserRepository $userRepository, CategoryRepository $categoryRepository): Response
    {
        $user = $userRepository->findOnebyCategory(8);
        $title = $categoryRepository->findCatName(8);
        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($user, $title);
        }

        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);
    }


}