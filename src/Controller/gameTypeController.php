<?php


namespace App\Controller;

use App\Repository\GameTypeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type")
 */
class gameTypeController extends AbstractController
{
    private $csvConverter;

    public function __construct(indexController $csvConverter)
    {
        $this->csvConverter = $csvConverter;
    }

    /**
     * @param UserRepository $userRepository
     * @param GameTypeRepository $gameTypeRepository
     * @return Response
     * @Route("/1", name="type_1", methods={"GET","POST"})
     */
    public function gameType1(UserRepository $userRepository, GameTypeRepository $gameTypeRepository): Response
    {
        $user = $userRepository->findOnebyGameType(1);
        $title = $gameTypeRepository->findGameTypeName(1);
        $longTitle[] = ['name'=>'Naam mensen die '. $title[0]['name'].' hebben gekozen'];

        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($user,$longTitle);
        }

        return $this->render('user/gameType.html.twig', [
            'users' => $user, 'topNames' =>$title
        ]);
    }

    /**
     * @param UserRepository $userRepository
     * @param GameTypeRepository $gameTypeRepository
     * @return Response
     * @Route("/2", name="type_2", methods={"GET","POST"})
     */
    public function gameType2(UserRepository $userRepository, GameTypeRepository $gameTypeRepository): Response
    {
        $user = $userRepository->findOnebyGameType(2);
        $title = $gameTypeRepository->findGameTypeName(2);


        if (isset($_POST['csv'])) {

            $this->csvConverter->makeCSV($user, $title);
        }

        return $this->render('user/gameType.html.twig', [
            'users' => $user, 'topNames' => $title
        ]);
    }
}
