<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\ActivityTypeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class indexController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET" ,"POST"})
     */
    public function index(UserRepository $userRepository, ActivityTypeRepository $activityTypeRepository): Response
    {
        $user = $userRepository->findOnebyActivity(1);
        $title = $activityTypeRepository->findactivityName(1);
        if (isset($_POST['csv'])){

            $this->makeCSV($user, $title);
        }
        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);

    }
    /**
     * @Route("/activity/1", name="act_1", methods={"GET","POST"})
     */
    public function act1(UserRepository $userRepository, ActivityTypeRepository $activityTypeRepository): Response
    {
        $user = $userRepository->findOnebyActivity(2);
        $title = $activityTypeRepository->findactivityName(2);
        if (isset($_POST['csv'])){

            $this->makeCSV($user, $title);
        }
        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);
    }
    /**
     * @Route("/activity/2", name="act_2", methods={"GET","POST"})
     */
    public function act2(UserRepository $userRepository, ActivityTypeRepository $activityTypeRepository): Response
    {
       $user = $userRepository->findOnebyActivity(3);
        $title = $activityTypeRepository->findactivityName(3);
        if (isset($_POST['csv'])){

            $this->makeCSV($user, $title);
        }
        return $this->render('user/index.html.twig', [
            'users' => $user , 'topNames' => $title
        ]);
    }
    /**
     * @Route("/name", name="name", methods={"GET","POST"})
     */
    public function allName(UserRepository $userRepository): Response
    {
        $user = $userRepository->findAllNames();
        $title[] = ['name'=>'naam'];
        if (isset($_POST['csv'])) {
            $this->makeCSV($user,$title);
        }
        return $this->render('user/name.html.twig', [
            'names' => $user
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }
        return $this->redirectToRoute('user_index');
    }
    public function makeCSV($data, $title){



        $file = fopen("dataCSV.csv",'w');
        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
            foreach ($title as $csvTitle){
                fputcsv($file, array_map('mb_strtoupper',$csvTitle));

        }
        foreach ( $data as $csv){
            $csvdata[] = $csv['name'];
            fputcsv($file,$csvdata);
            $csvdata = [];
        }

        fclose($file);

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=".$title[0]['name'].".csv");
        header("Pragma: no-cache");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Expires: 0");
        readfile('dataCSV.csv');
        unlink('dataCSV.csv');
        exit();
    }


}