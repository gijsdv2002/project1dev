<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200420141056 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE activity_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE admin (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_880E0D76E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, gametype_id_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_64C19C1EE7279BF (gametype_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, activity_type_id_id INT NOT NULL, game_type_id_id INT NOT NULL, category_id_id INT NOT NULL, name VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, zipcode VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, email_address VARCHAR(255) NOT NULL, INDEX IDX_8D93D64924AA27A8 (activity_type_id_id), INDEX IDX_8D93D6499F94DE4D (game_type_id_id), INDEX IDX_8D93D6499777D11E (category_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1EE7279BF FOREIGN KEY (gametype_id_id) REFERENCES game_type (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64924AA27A8 FOREIGN KEY (activity_type_id_id) REFERENCES activity_type (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6499F94DE4D FOREIGN KEY (game_type_id_id) REFERENCES game_type (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6499777D11E FOREIGN KEY (category_id_id) REFERENCES category (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64924AA27A8');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6499777D11E');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1EE7279BF');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6499F94DE4D');
        $this->addSql('DROP TABLE activity_type');
        $this->addSql('DROP TABLE admin');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE game_type');
        $this->addSql('DROP TABLE user');
    }
}
