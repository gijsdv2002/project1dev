<?php

namespace App\Form;

use App\Entity\ActivityType;
use App\Entity\Category;
use App\Entity\GameType;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name',TextType::class, ['label' => 'Naam'])
            ->add('address',TextType::class, ['label' => 'Adres'])
            ->add('zipcode',TextType::class, ['label' => 'Postcode'])
            ->add('city',TextType::class, ['label' => 'Stad'])
            ->add('emailAddress', EmailType::class, ['label' => 'Email'])
            ->add('ActivityTypeId', EntityType::class,
                ['class' => ActivityType::class,
                    'label' => 'Hoe speel je?',
                    'choice_label' => 'name',
                    'placeholder' => 'Kies een optie',])
            ->add('GameTypeId', EntityType::class,
                ['class' => GameType::class,
                'label' => 'Speel je fysiek of online?',
                    'choice_label' => 'name',
                    'attr' => ['class' => 'Typ'],
                    'placeholder' => 'Kies een optie',])
            ->add('CategoryId', EntityType::class,
                ['class' => Category::class,
                    'label' => 'Welk soort categorie spel speel je?',
                    'choice_label' => 'name',
                    'choice_attr' => function ($choice, $key, $value) {
                        switch ($value) {
                            case 1:
                                return ['class' => 'Bord'];
                                break;
                            case 2:
                                return ['class' => 'Bord'];
                                break;
                            case 3:
                                return ['class' => 'Bord'];
                                break;
                            case 4:
                                return ['class' => 'Bord'];
                                break;
                            default:
                                return ['class' => 'Comp'];

                        }
                    },
                    'placeholder' => 'Kies een optie',
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
