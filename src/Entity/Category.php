<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="CategoryId")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GameType", inversedBy="GameTypeId")
     * @ORM\JoinColumn(nullable=false)
     */
    private $GametypeId;



    public function __construct()
    {
        $this->id = new ArrayCollection();
    }

    /**
     * * @return Collection|User[]
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGametypeId(): ?GameType
    {
        return $this->GametypeId;
    }

    public function setGametypeId(?GameType $GametypeId): self
    {
        $this->GametypeId = $GametypeId;

        return $this;
    }

}
