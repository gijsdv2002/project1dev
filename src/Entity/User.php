<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $zipcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $emailAddress;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ActivityType", inversedBy="id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ActivityTypeId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GameType", inversedBy="id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $GameTypeId;

    private $category;

    public function getCategory() : ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $CategoryId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getEmailAddress(): ?string
    {
        return $this->emailAddress;
    }

    public function setEmailAddress(string $emailAddress): self
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    public function getActivityTypeId(): ?ActivityType
    {
        return $this->ActivityTypeId;
    }

    public function setActivityTypeId(?ActivityType $ActivityTypeId): self
    {
        $this->ActivityTypeId = $ActivityTypeId;

        return $this;
    }

    public function getGameTypeId(): ?GameType
    {
        return $this->GameTypeId;
    }

    public function setGameTypeId(?GameType $GameTypeId): self
    {
        $this->GameTypeId = $GameTypeId;

        return $this;
    }

    public function getCategoryId(): ?Category
    {
        return $this->CategoryId;
    }

    public function setCategoryId(?Category $CategoryId): self
    {
        $this->CategoryId = $CategoryId;

        return $this;
    }
}
