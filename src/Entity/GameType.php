<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameTypeRepository")
 */
class GameType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="GameTypeId")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Category", mappedBy="GametypeId")
     */
    private $GameTypeId;

    public function __construct()
    {
        $this->id = new ArrayCollection();
        $this->GameTypeId = new ArrayCollection();
    }

    /**
     * @return Collection|User[]
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getGameTypeId(): Collection
    {
        return $this->GameTypeId;
    }

    public function addGameTypeId(Category $gameTypeId): self
    {
        if (!$this->GameTypeId->contains($gameTypeId)) {
            $this->GameTypeId[] = $gameTypeId;
            $gameTypeId->setGametypeId($this);
        }

        return $this;
    }

    public function removeGameTypeId(Category $gameTypeId): self
    {
        if ($this->GameTypeId->contains($gameTypeId)) {
            $this->GameTypeId->removeElement($gameTypeId);
            // set the owning side to null (unless already changed)
            if ($gameTypeId->getGametypeId() === $this) {
                $gameTypeId->setGametypeId(null);
            }
        }

        return $this;
    }



}
