<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */

    public function findOnebyActivity($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.ActivityTypeId = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
        ;
    }
    public function findAllNames()
    {
        return $this->createQueryBuilder('u')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
            ;
    }
    public function findOnebyCategory($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.CategoryId = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
            ;
    }
    public function findOnebyGameType($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.GameTypeId = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
            ;
    }
}
