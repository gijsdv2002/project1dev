<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Adminfixtures extends Fixture
{
    private $passwordEncode;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->passwordEncode = $encoder;
    }

    public function load(ObjectManager $manager)
    {

         $product = new Admin();
         $product->setEmail('jan@mail.mail');
         $product->setRoles(['ROLES_ADMIN']);
         $product->setPassword($this->passwordEncode->encodePassword($product,'test'));
         $manager->persist($product);

        $manager->flush();
    }
}
